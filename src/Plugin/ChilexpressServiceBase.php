<?php

namespace Drupal\chilexpress\Plugin;

use Drupal\Component\Plugin\PluginBase;

/**
 * Base class for Chilexpress service plugins.
 */
abstract class ChilexpressServiceBase extends PluginBase implements ChilexpressServiceInterface {


  // Add common methods and abstract methods for your plugin type here.

}
