<?php

namespace Drupal\chilexpress\Plugin;

use Drupal\Core\Plugin\DefaultPluginManager;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;

/**
 * Provides the Chilexpress service plugin manager.
 */
class ChilexpressServiceManager extends DefaultPluginManager {


  /**
   * Constructs a new ChilexpressServiceManager object.
   *
   * @param \Traversable $namespaces
   *   An object that implements \Traversable which contains the root paths
   *   keyed by the corresponding namespace to look for plugin implementations.
   * @param \Drupal\Core\Cache\CacheBackendInterface $cache_backend
   *   Cache backend instance to use.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler to invoke the alter hook with.
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    parent::__construct('Plugin/ChilexpressService', $namespaces, $module_handler, 'Drupal\chilexpress\Plugin\ChilexpressServiceInterface', 'Drupal\chilexpress\Annotation\ChilexpressService');

    $this->alterInfo('chilexpress_service_info');
    $this->setCacheBackend($cache_backend, 'chilexpress_service_plugins');
  }

}
