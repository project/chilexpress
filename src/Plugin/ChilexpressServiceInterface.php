<?php

namespace Drupal\chilexpress\Plugin;

use Drupal\Component\Plugin\PluginInspectionInterface;

/**
 * Defines an interface for Chilexpress service plugins.
 */
interface ChilexpressServiceInterface extends PluginInspectionInterface {


  // Add get/set methods for your plugin type here.

}
