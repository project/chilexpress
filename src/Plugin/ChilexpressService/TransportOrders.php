<?php


namespace Drupal\chilexpress\Plugin\ChilexpressService;

use Drupal\chilexpress\Plugin\ChilexpressServiceBase;

/**
 * Defines the Transport orders service.
 *
 * @ChilexpressService(
 *   id = "transport_orders",
 *   label = @Translation("Transport orders"),
 *   weight = 3,
 * )
 */
class TransportOrders extends ChilexpressServiceBase {

}
