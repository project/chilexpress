<?php


namespace Drupal\chilexpress\Plugin\ChilexpressService;

use Drupal\chilexpress\Plugin\ChilexpressServiceBase;

/**
 * Defines the Rating service.
 *
 * @ChilexpressService(
 *   id = "rating",
 *   label = @Translation("Rating"),
 *   weight = 2,
 * )
 */
class Rating extends ChilexpressServiceBase {

}
