<?php


namespace Drupal\chilexpress\Plugin\ChilexpressService;

use Drupal\chilexpress\Plugin\ChilexpressServiceBase;

/**
 * Defines the Georeference service.
 *
 * @ChilexpressService(
 *   id = "georeference",
 *   label = @Translation("Georeference"),
 *   weight = 1,
 * )
 */
class Georeference extends ChilexpressServiceBase {

}
