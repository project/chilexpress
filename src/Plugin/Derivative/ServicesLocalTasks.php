<?php


namespace Drupal\chilexpress\Plugin\Derivative;


use Drupal\chilexpress\Plugin\ChilexpressServiceManager;
use Drupal\Component\Plugin\Derivative\DeriverBase;
use Drupal\Core\Plugin\Discovery\ContainerDeriverInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ServicesLocalTasks extends DeriverBase implements ContainerDeriverInterface {

  /**
   * @var \Drupal\chilexpress\Plugin\ChilexpressServiceManager
   */
  protected $chilexpressServiceManager;

  /**
   * ServicesLocalTasks constructor.
   *
   * @param \Drupal\chilexpress\Plugin\ChilexpressServiceManager $chilexpress_service_manager
   */
  public function __construct(ChilexpressServiceManager $chilexpress_service_manager) {
    $this->chilexpressServiceManager = $chilexpress_service_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, $base_plugin_id) {
    return new static(
      $container->get('plugin.manager.chilexpress_service')
    );
  }


  /**
   * {@inheritdoc}
   */
  public function getDerivativeDefinitions($base_plugin_definition) {

    foreach ($this->chilexpressServiceManager->getDefinitions() as $service_id => $definition) {
      $this->derivatives['chilexpress.service_settings_form.' . $service_id] = $base_plugin_definition;
      $this->derivatives['chilexpress.service_settings_form.' . $service_id]['title'] = $definition['label'];
      $this->derivatives['chilexpress.service_settings_form.' . $service_id]['weight'] = $definition['weight'];
      $this->derivatives['chilexpress.service_settings_form.' . $service_id]['route_name'] = 'chilexpress.service_settings_form.' . $service_id;
    }

    return parent::getDerivativeDefinitions($base_plugin_definition);
  }



}
