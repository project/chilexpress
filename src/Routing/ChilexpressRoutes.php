<?php


namespace Drupal\chilexpress\Routing;

use Drupal\chilexpress\Plugin\ChilexpressServiceManager;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Route;

/**
 * Class RouteSubscriber
 *
 * @package Drupal\chilexpress\EventSubscriber
 */
class ChilexpressRoutes implements ContainerInjectionInterface {

  /**
   * @var \Drupal\chilexpress\Plugin\ChilexpressServiceManager
   */
  protected $chilexpressServiceManager;

  /**
   * RouteSubscriber constructor.
   *
   * @param \Drupal\chilexpress\Plugin\ChilexpressServiceManager $chilexpress_service_manager
   */
  public function __construct(ChilexpressServiceManager $chilexpress_service_manager) {
    $this->chilexpressServiceManager = $chilexpress_service_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('plugin.manager.chilexpress_service')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function routes() {
    $routes = [];

    foreach ($this->chilexpressServiceManager->getDefinitions() as $service_id => $definition) {
      $routes['chilexpress.service_settings_form.' . $service_id] = new Route(
        '/admin/config/services/chilexpress/' . $service_id,
        [
          '_form' => '\Drupal\chilexpress\Form\ServiceSettingsForm',
          'service' => $service_id,
          '_title_callback' => '\Drupal\chilexpress\Controller\ChilexpressController::serviceLabel',
        ],
        [
          '_permission'  => 'administer site configuration',
        ]
      );
    }


    return $routes;
  }

}
