<?php

namespace Drupal\chilexpress\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Config\Config;

/**
 * Class ServiceSettingsForm.
 */
class ServiceSettingsForm extends ConfigFormBase {

  /**
   * @var string $service
   */
  protected $service;

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'chilexpress.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chilexpress_service_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, $service = NULL) {
    $this->service = $service;

    $config = $this->config('chilexpress.settings');

    $form['active_environment'] = [
      '#type' => 'radios',
      '#title' => $this->t('Active Environment'),
      '#options' => [
        'testing' => $this->t('Testing'),
        'production' => $this->t('Production'),
      ],
      '#default_value' => $config->get($service . '.active_environment'),
      '#required' => TRUE,
    ];
    $form['testing'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Testing'),
      '#tree' => TRUE,
    ]  + $this->buildEnvironmentForm($config, 'testing');

    $form['production'] = [
        '#type' => 'fieldset',
        '#title' => $this->t('Production'),
        '#tree' => TRUE,
      ] + $this->buildEnvironmentForm($config, 'production');

    return parent::buildForm($form, $form_state);
  }

  /**
   * Return elements for environment setting.
   *
   * @param \Drupal\Core\Config\Config $config
   *  The config object.
   * @param string $environment
   *  The environment name.
   *
   * @return array
   */
  protected function buildEnvironmentForm(Config $config, $environment) {
    return [
      'url' => [
        '#type' => 'url',
        '#title' => $this->t('Url'),
        '#default_value' =>  $config->get($this->service . '.' . $environment . '.url'),
        '#required' => TRUE,
      ],
      'subscription_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('Subscription key'),
        '#default_value' =>  $config->get($this->service . '.' . $environment . '.subscription_key'),
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $this->config('chilexpress.settings')
      ->set($this->service . '.active_environment', $form_state->getValue('active_environment'))
      ->set($this->service . '.testing', $form_state->getValue('testing'))
      ->set($this->service . '.production', $form_state->getValue('production'))
      ->save();

  }

}
