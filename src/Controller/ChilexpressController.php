<?php

namespace Drupal\chilexpress\Controller;

use Drupal\chilexpress\Plugin\ChilexpressServiceManager;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class ChilexpressController.
 */
class ChilexpressController extends ControllerBase {

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * @var \Drupal\chilexpress\Plugin\ChilexpressServiceManager
   */
  protected $chilexpressServiceManager;

  /**
   * ChilexpressController constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   * @param \Drupal\chilexpress\Plugin\ChilexpressServiceManager $chilexpress_service_mananger
   */
  public function __construct(ConfigFactoryInterface $config_factory, ChilexpressServiceManager $chilexpress_service_mananger) {
    $this->config = $config_factory->get('chilexpress.settings');
    $this->chilexpressServiceManager = $chilexpress_service_mananger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('plugin.manager.chilexpress_service')
    );
  }

  /**
   * Overview.
   *
   * @return array.
   *   A renderable array of the chilexpress overview page.
   */
  public function overview() {
    $build = [
      '#type' => 'table',
      '#header' => [
        $this->t('Service'),
        $this->t('Active Environment'),
        $this->t('Operations'),
      ],
    ];

    foreach ($this->chilexpressServiceManager->getDefinitions() as $service_id => $definition) {
      $build[$service_id]['#weight'] = $definition['weight'];
      $build[$service_id]['service'] = [
        '#markup' => $definition['label'],
      ];
      $build[$service_id]['active_environment'] = [
        '#markup' => $this->config->get($service_id . '.active_environment'),
      ];
      $build[$service_id]['operations'] = [
        '#type' => 'operations',
        '#links' => [
          'edit' => [
            'title' => $this->t('Edit'),
            'url' => Url::fromRoute('chilexpress.service_settings_form.' . $service_id),
          ],
        ],
      ];
    }

    return $build;
  }

  /**
   * @param $service
   *
   * @return mixed
   */
  public function serviceLabel($service) {
    try {
      $definition = $this->chilexpressServiceManager->getDefinition($service);

      return $definition['label'];
    } catch (PluginNotFoundException $e) {

      return '';
    }
  }
}
