<?php

namespace Drupal\chilexpress\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Chilexpress service item annotation object.
 *
 * @see \Drupal\chilexpress\Plugin\ChilexpressServiceManager
 * @see plugin_api
 *
 * @Annotation
 */
class ChilexpressService extends Plugin {


  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

  /**
   * The weight of the plugin
   *
   * @var int
   */
  public $weight = 0;
}
